package consolaJava;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		int opcion;
		
		do {
			System.out.println("*********************");
			System.out.println("1. OPCI�N 1. PAR O IMPAR");
			System.out.println("2. OPCI�N 2. ABSOLUTO");
			System.out.println("3. OPCI�N 3. MAYOR");
			System.out.println("4. OPCI�N 4. M�NIMO");
			System.out.println("5. SALIR");
			System.out.println("*********************");
			opcion = lector.nextInt();
			
			switch (opcion) {
				case 1:
					System.out.println("Introduce un n�mero");
					int numero1 = lector.nextInt();
					parImpar(numero1);
					break;
					
				case 2:
					System.out.println("Introduce un n�mero");
					int numero2 = lector.nextInt();
					absoluto(numero2);
					break;
					
				case 3:
					int[] numeros3 = new int[2];
					for (int i = 0; i < numeros3.length; i++) {
						System.out.println("Introduce un n�mero (" + (i + 1) + "/2)");
						numeros3[i] = lector.nextInt();
					}
					mayor(numeros3);
					break;
					
				case 4:
					int[] numeros4 = new int[2];
					for (int i = 0; i < numeros4.length; i++) {
						System.out.println("Introduce un n�mero (" + (i + 1) + "/2)");
						numeros4[i] = lector.nextInt();
					}
					menor(numeros4);
					break;
					
				case 5:
					System.out.println("Has salido del programa");
					break;
					
				default:
					System.out.println("No has introducido una de las 4 opciones");
					break;
			}
		}
		while (opcion != 5);
		
		lector.close();

	}
	
	static void parImpar(int numero1) {
		if(numero1 % 2 == 0) {
			System.out.println("El " + numero1 + " es par");
		}
		else {
			System.out.println("El " + numero1 + " es impar");
		}
	}
	
	static void absoluto(int numero2) {
		if(numero2 > 0) {
			System.out.println("El " + numero2 + " es absoluto");
		}
		else {
			System.out.println("El " + numero2 + " no es absoluto");
		}
	}
	
	static void mayor(int[] numeros3) {
			if(numeros3[0] > numeros3[1]) {
				System.out.println("El " + numeros3[0] + " es el n�mero mayor");
			}
			else {
				System.out.println("El " + numeros3[1] + " es el n�mero mayor");
			}
	}
	
	static void menor(int[] numeros4) {
		if(numeros4[0] < numeros4[1]) {
			System.out.println("El " + numeros4[0] + " es el n�mero menor");
		}
		else {
			System.out.println("El " + numeros4[1] + " es el n�mero menor");
		}
	}
}
