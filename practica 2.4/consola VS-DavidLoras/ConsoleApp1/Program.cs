﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVS
{
    class Program
    {
        static void Main(string[] args)
        {
            String opcion;
            do
            {
                Console.WriteLine("*********************");
                Console.WriteLine("1. OPCIÓN 1. PAR O IMPAR");
                Console.WriteLine("2. OPCIÓN 2. ABSOLUTO");
                Console.WriteLine("3. OPCIÓN 3. MAYOR");
                Console.WriteLine("4. OPCIÓN 4. MÍNIMO");
                Console.WriteLine("5. SALIR");
                Console.WriteLine("*********************");
                opcion = Console.ReadLine();

                switch (opcion)
                {
                    case "1":
                        Console.WriteLine("Introduce un número:");
                        String cadena1 = Console.ReadLine();
                        int numero1 = int.Parse(cadena1);
                        parImpar(numero1);
                        break;

                    case "2":
                        Console.WriteLine("Introduce un número:");
                        String cadena2 = Console.ReadLine();
                        int numero2 = int.Parse(cadena2);
                        absoluto(numero2);
                        break;

                    case "3":
                        int[] numeros3 = new int[2];
                        for (int i = 0; i < numeros3.Length; i++)
                        {
                            Console.WriteLine("Introduce un número (" + (i + 1) + "/2):");
                            String cadena3 = Console.ReadLine();
                            numeros3[i] = int.Parse(cadena3);
                        }
                        mayor(numeros3);
                        break;

                    case "4":
                        int[] numeros4 = new int[2];
                        for (int i = 0; i < numeros4.Length; i++)
                        {
                            Console.WriteLine("Introduce un número (" + (i + 1) + "/2):");
                            String cadena3 = Console.ReadLine();
                            numeros4[i] = int.Parse(cadena3);
                        }
                        menor(numeros4);
                        break;

                    case "5":
                        Console.WriteLine("Has salido del programa");
                        break;

                    default:
                        Console.WriteLine("No has introducido ninguna opción");
                        break;
                }
            }
            while (!opcion.Equals("5"));
        }

        static void parImpar(int numero1)
        {
            if (numero1 % 2 == 0)
            {
                Console.WriteLine("El " + numero1 + " es par");
            }
            else
            {
                Console.WriteLine("El " + numero1 + " es impar");
            }
        }

        static void absoluto(int numero2)
        {
            if (numero2 > 0)
            {
                Console.WriteLine("El " + numero2 + " es absoluto");
            }
            else
            {
                Console.WriteLine("El " + numero2 + " no es absoluto");
            }
        }

        static void mayor(int[] numeros3)
        {
            if (numeros3[0] > numeros3[1])
            {
                Console.WriteLine("El " + numeros3[0] + " es el número mayor");
            }
            else
            {
                Console.WriteLine("El " + numeros3[1] + " es el número mayor");
            }
        }

        static void menor(int[] numeros4)
        {
            if (numeros4[0] < numeros4[1])
            {
                Console.WriteLine("El " + numeros4[0] + " es el número menor");
            }
            else
            {
                Console.WriteLine("El " + numeros4[1] + " es el número menor");
            }
        }
    }
}