package pruebaLibreriaJava;

import libreriaJava.Cadenas;
import libreriaJava.Numeros;

public class PruebaLibreriaJava {

	public static void main(String[] args) {
		
        Numeros.aletorio(100);
        Numeros.exponente(2, 5);
        Numeros.parImpar(7);
        Numeros.perfecto(28);
        Numeros.primo(13);

        Cadenas.buscarLetra("Programacion", 'a');
        Cadenas.cadenasIguales("Sistemas", "Informaticos");
        Cadenas.consonantes("Bases de datos");
        Cadenas.inversa("Java");
        Cadenas.vocales("Visual Studio");

	}

}
