package libreriaJava;

public class Numeros {
	
	public static void primo(int numero) {
		int contador = 0;
		for (int i = 1; i < numero; i++) {
			if(numero % i == 0) {
				contador++;
			}
		}
		if (contador == 2) {
			System.out.println("El n�mero " + numero + " es primo");
		}
		else {
			System.out.println("El n�mero " + numero + " no es primo");
		}
	}
	
	public static void perfecto(int numero) {
		int suma = 0;
		for (int i = 1; i < numero - 1; i++) {
			if (numero % i == 0) {
				suma = suma + i;
			}
		}
		if (suma == numero) {
			System.out.println("El n�mero " + numero + " es perfecto");
		}
		else {
			System.out.println("El n�mero " + numero + " no es perfecto");
		}
	}
	
	public static void parImpar(int numero) {
		if(numero % 2 == 0) {
			System.out.println("El n�mero " + numero + " es par");
		}
		else {
			System.out.println("El n�mero " + numero + " es impar");
		}
	}
	
	public static void exponente(int numero, int exponente) {
		int total = 1;
		for (int i = 0; i < exponente; i++) {
			total = total * numero;
		}
		System.out.println("El n�mero " + numero + " elevado a " + exponente + " es " + total);
	}
	
	public static void aletorio(int numero) {
		System.out.println("N�mero aleatorio del 0 al " + numero + ": " + (int)(Math.random()*(numero + 1)));
	}
}
