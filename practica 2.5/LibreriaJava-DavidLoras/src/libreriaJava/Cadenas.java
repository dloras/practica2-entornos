package libreriaJava;

public class Cadenas {

	public static void inversa (String cadena) {
		String cadenaInversa = "";
		char caracter;
		for (int i = cadena.length() - 1; i >= 0 ; i--) {
			caracter = cadena.charAt(i);
			cadenaInversa = cadenaInversa + caracter;
		}
		System.out.println("La cadena inversa de " + cadena + " es: " + cadenaInversa);
	}
	
	public static void vocales (String cadena) {
		cadena = cadena.toLowerCase();
		int contadorVocales = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'u') {
				contadorVocales++;
			}
		}
		System.out.println("En la cadena " + cadena + " hay " + contadorVocales + " vocales");
	}
	
	public static void consonantes (String cadena) {
		cadena = cadena.toLowerCase();
		int contadorConsonantes = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) >= 'a' && cadena.charAt(i) <= 'a') {
				contadorConsonantes++;
			}
		}
		System.out.println("En la cadena " + cadena + " hay " + contadorConsonantes + " consonantes");
	}
	
	public static void cadenasIguales (String cadena1, String cadena2) {
		if (cadena1.equalsIgnoreCase(cadena2)) {
			System.out.println("Las cadenas " + cadena1 + " y " + cadena2 + " son iguales");
		}
		else {
			System.out.println("Las cadenas " + cadena1 + " y " + cadena2 + " no son iguales");
		}
	}
	
	public static void buscarLetra (String cadena, char letra) {
		String cadena2 = cadena.toLowerCase();
		if (cadena2.indexOf(letra) >= 0) {
			System.out.println("La cadena " + cadena + " contiene la letra " + letra);
		}
		else {
			System.out.println("La cadena " + cadena + " no contiene la letra " + letra);
		}
	}

}
