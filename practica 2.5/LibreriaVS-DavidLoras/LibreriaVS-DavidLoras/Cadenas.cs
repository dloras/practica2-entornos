﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_DavidLoras
{
    public class Cadenas
    {
        public static void inversa(String cadena)
        {
            String cadenaInversa = "";
            char caracter;
            for (int i = cadena.Length - 1; i >= 0; i--)
            {
                caracter = cadena[i];
                cadenaInversa = cadenaInversa + caracter;
            }
            Console.WriteLine("La cadena inversa de " + cadena + " es: " + cadenaInversa);
        }

        public static void vocales(String cadena)
        {
            cadena = cadena.ToLower();
            int contadorVocales = 0;
            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] == 'a' || cadena[i] == 'e' || cadena[i] == 'i' || cadena[i] == 'o' || cadena[i] == 'u')
                {
                    contadorVocales++;
                }
            }
            Console.WriteLine("En la cadena " + cadena + " hay " + contadorVocales + " vocales");
        }

        public static void consonantes(String cadena)
        {
            cadena = cadena.ToLower();
            int contadorConsonantes = 0;
            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] >= 'a' && cadena[i] <= 'a')
                {
                    contadorConsonantes++;
                }
            }
            Console.WriteLine("En la cadena " + cadena + " hay " + contadorConsonantes + " consonantes");
        }

        public static void cadenasIguales(String cadena1, String cadena2)
        {
            if (cadena1.Equals(cadena2))
            {
                Console.WriteLine("Las cadenas " + cadena1 + " y " + cadena2 + " son iguales");
            }
            else
            {
                Console.WriteLine("Las cadenas " + cadena1 + " y " + cadena2 + " no son iguales");
            }
        }

        public static void buscarLetra(String cadena, char letra)
        {
            String cadena2 = cadena.ToLower();
            if (cadena2.IndexOf(letra) >= 0)
            {
                Console.WriteLine("La cadena " + cadena + " contiene la letra " + letra);
            }
            else
            {
                Console.WriteLine("La cadena " + cadena + " no contiene la letra " + letra);
            }
        }
    }
}
