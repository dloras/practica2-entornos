﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_DavidLoras
{
    public class Numeros
    {
        public static void primo(int numero)
        {
            int contador = 0;
            for (int i = 1; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    contador++;
                }
            }
            if (contador == 2)
            {
                Console.WriteLine("El número " + numero + " es primo");
            }
            else
            {
                Console.WriteLine("El número " + numero + " no es primo");
            }
        }

        public static void perfecto(int numero)
        {
            int suma = 0;
            for (int i = 1; i < numero - 1; i++)
            {
                if (numero % i == 0)
                {
                    suma = suma + i;
                }
            }
            if (suma == numero)
            {
                Console.WriteLine("El número " + numero + " es perfecto");
            }
            else
            {
                Console.WriteLine("El número " + numero + " no es perfecto");
            }
        }

        public static void parImpar(int numero)
        {
            if (numero % 2 == 0)
            {
                Console.WriteLine("El número " + numero + " es par");
            }
            else
            {
                Console.WriteLine("El número " + numero + " es impar");
            }
        }

        public static void exponente(int numero, int exponente)
        {
            int total = 1;
            for (int i = 0; i < exponente; i++)
            {
                total = total * numero;
            }
            Console.WriteLine("El número " + numero + " elevado a " + exponente + " es " + total);
        }

        public static void aletorio(int numero)
        {
            Random numeroAleatorio = new Random();
            Console.WriteLine("Número aleatorio del 0 al " + numero + ": " + numeroAleatorio.Next(0, numero));
        }
    }
}
