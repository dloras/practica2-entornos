﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaLibreria
{
    class Program
    {
        static void Main(string[] args)
        {
            LibreriaVS_DavidLoras.Numeros.aletorio(100);
            LibreriaVS_DavidLoras.Numeros.exponente(2, 5);
            LibreriaVS_DavidLoras.Numeros.parImpar(7);
            LibreriaVS_DavidLoras.Numeros.perfecto(28);
            LibreriaVS_DavidLoras.Numeros.primo(13);

            LibreriaVS_DavidLoras.Cadenas.buscarLetra("Programacion", 'a');
            LibreriaVS_DavidLoras.Cadenas.cadenasIguales("Sistemas", "Informaticos");
            LibreriaVS_DavidLoras.Cadenas.consonantes("Bases de datos");
            LibreriaVS_DavidLoras.Cadenas.inversa("Java");
            LibreriaVS_DavidLoras.Cadenas.vocales("Visual Studio");

            Console.WriteLine("Pulsa una tecla para terminar");
            Console.ReadKey();
        }
    }
}
