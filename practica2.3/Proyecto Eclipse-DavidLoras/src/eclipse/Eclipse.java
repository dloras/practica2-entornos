package eclipse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.SpinnerNumberModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JToolBar;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import javax.swing.JSlider;
import java.awt.TextArea;
import javax.swing.JMenuItem;
import java.awt.Button;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

/**
 * 
 * @author David
 * @since 21/12/2017
 * 
 */

public class Eclipse extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eclipse frame = new Eclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eclipse() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Eclipse.class.getResource("/imagenes/film-16.png")));
		setTitle("B\u00FAsquedaApp");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 21);
		contentPane.add(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNuevaBsqueda = new JMenuItem("Nueva");
		mntmNuevaBsqueda.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/new-badge-16.png")));
		mnArchivo.add(mntmNuevaBsqueda);
		
		JMenuItem mntmAbrirBsqueda = new JMenuItem("Abrir");
		mntmAbrirBsqueda.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/browser-16.png")));
		mnArchivo.add(mntmAbrirBsqueda);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mntmGuardar.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/save-16.png")));
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como...");
		mntmGuardarComo.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/save-as-16.png")));
		mnArchivo.add(mntmGuardarComo);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mntmCerrar.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/cancel-16.png")));
		mnArchivo.add(mntmCerrar);
		
		JMenu mnImprimir = new JMenu("Imprimir...");
		mnImprimir.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/printer-16.png")));
		mnArchivo.add(mnImprimir);
		
		JMenuItem mntmImprimir = new JMenuItem("Imprimir");
		mnImprimir.add(mntmImprimir);
		
		JMenuItem mntmConfigurarPgina = new JMenuItem("Configurar p\u00E1gina");
		mnImprimir.add(mntmConfigurarPgina);
		
		JMenuItem mntmVistaPrevia = new JMenuItem("Vista previa");
		mnImprimir.add(mntmVistaPrevia);
		
		JMenuItem mntmEnviar = new JMenuItem("Enviar...");
		mntmEnviar.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/mail-16.png")));
		mnArchivo.add(mntmEnviar);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/power-16.png")));
		mnArchivo.add(mntmSalir);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mntmDeshacer.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/arrow-87-16.png")));
		mnEdicin.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mntmRehacer.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/arrow-55-16.png")));
		mnEdicin.add(mntmRehacer);
		
		JMenu mnVer = new JMenu("Ver");
		menuBar.add(mnVer);
		
		JMenuItem mntmPantallaCompleta = new JMenuItem("Pantalla Completa");
		mntmPantallaCompleta.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/desktop-16.png")));
		mnVer.add(mntmPantallaCompleta);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmSoporteTcnico = new JMenuItem("Soporte t\u00E9cnico");
		mntmSoporteTcnico.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/tool-box-16.png")));
		mnAyuda.add(mntmSoporteTcnico);
		
		JMenuItem mntmObtenerInformacin = new JMenuItem("Informaci\u00F3n del programa");
		mntmObtenerInformacin.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/info-2-16.png")));
		mnAyuda.add(mntmObtenerInformacin);
		
		JMenuItem mntmBuscarActualizaciones = new JMenuItem("Buscar actualizaciones");
		mntmBuscarActualizaciones.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/downloading-updates-16.png")));
		mnAyuda.add(mntmBuscarActualizaciones);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 21, 434, 29);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/new-badge-16.png")));
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/browser-16.png")));
		toolBar.add(btnNewButton_6);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/save-16.png")));
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/save-as-16.png")));
		toolBar.add(btnNewButton_2);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/printer-16.png")));
		toolBar.add(btnNewButton_5);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/arrow-87-16.png")));
		toolBar.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/arrow-55-16.png")));
		toolBar.add(btnNewButton_4);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 50, 414, 250);
		contentPane.add(tabbedPane);
		
		JPanel B�squeda = new JPanel();
		B�squeda.setBackground(SystemColor.inactiveCaption);
		B�squeda.setToolTipText("");
		tabbedPane.addTab("B�squeda avanzada", null, B�squeda, null);
		B�squeda.setLayout(null);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/search-2-16.png")));
		btnBuscar.setBounds(294, 188, 105, 23);
		B�squeda.add(btnBuscar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/cancel-16.png")));
		btnCancelar.setBounds(179, 188, 105, 23);
		B�squeda.add(btnCancelar);
		
		JLabel lblTexto = new JLabel("T\u00EDtulo:");
		lblTexto.setBounds(10, 14, 46, 14);
		B�squeda.add(lblTexto);
		
		textField = new JTextField();
		textField.setBounds(66, 11, 333, 20);
		B�squeda.add(textField);
		textField.setColumns(10);
		
		JLabel lblFormato = new JLabel("Formato: ");
		lblFormato.setToolTipText("Formato");
		lblFormato.setBounds(10, 42, 75, 14);
		B�squeda.add(lblFormato);
		
		JRadioButton rdbtnDvd = new JRadioButton("DVD");
		rdbtnDvd.setToolTipText("Formato");
		rdbtnDvd.setBackground(SystemColor.inactiveCaption);
		rdbtnDvd.setBounds(66, 38, 68, 23);
		B�squeda.add(rdbtnDvd);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Blu-ray");
		rdbtnNewRadioButton.setBackground(SystemColor.inactiveCaption);
		rdbtnNewRadioButton.setToolTipText("Formato");
		rdbtnNewRadioButton.setBounds(136, 38, 88, 23);
		B�squeda.add(rdbtnNewRadioButton);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(SystemColor.window);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Acci\u00F3n", "Comedia", "Ciencia Ficci\u00F3n", "Terror", "Fantas\u00EDa", "Musical", "Drama", "Rom\u00E1ntica", "Suspense"}));
		comboBox.setBounds(66, 89, 117, 20);
		B�squeda.add(comboBox);
		
		JLabel lblAoDeEstreno = new JLabel("A\u00F1o de estreno:");
		lblAoDeEstreno.setBounds(10, 67, 89, 14);
		B�squeda.add(lblAoDeEstreno);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(2017), null, null, new Integer(1)));
		spinner.setBounds(108, 64, 75, 20);
		B�squeda.add(spinner);
		
		JLabel lblGnero = new JLabel("G\u00E9nero:");
		lblGnero.setBounds(10, 92, 46, 14);
		B�squeda.add(lblGnero);
		
		JCheckBox chckbxFiccin = new JCheckBox("Series");
		chckbxFiccin.setBackground(SystemColor.inactiveCaption);
		chckbxFiccin.setBounds(66, 116, 64, 23);
		B�squeda.add(chckbxFiccin);
		
		JCheckBox chckbxDocumental = new JCheckBox("Documentales");
		chckbxDocumental.setBackground(SystemColor.inactiveCaption);
		chckbxDocumental.setBounds(127, 116, 97, 23);
		B�squeda.add(chckbxDocumental);
		
		JLabel lblExcluir = new JLabel("Excluir:");
		lblExcluir.setBounds(10, 120, 46, 14);
		B�squeda.add(lblExcluir);
		
		JRadioButton rdbtnVhs = new JRadioButton("VHS");
		rdbtnVhs.setToolTipText("Formato");
		rdbtnVhs.setBackground(SystemColor.inactiveCaption);
		rdbtnVhs.setBounds(228, 38, 75, 23);
		B�squeda.add(rdbtnVhs);
		
		JRadioButton rdbtnEnCine = new JRadioButton("Cine");
		rdbtnEnCine.setToolTipText("Formato");
		rdbtnEnCine.setBackground(SystemColor.inactiveCaption);
		rdbtnEnCine.setBounds(317, 38, 82, 23);
		B�squeda.add(rdbtnEnCine);
		
		JSlider slider = new JSlider();
		slider.setBackground(SystemColor.inactiveCaption);
		slider.setBounds(157, 140, 208, 26);
		B�squeda.add(slider);
		
		JLabel lblResultadosPorPgina = new JLabel("Resultados p\u00E1gina:");
		lblResultadosPorPgina.setBounds(10, 145, 124, 14);
		B�squeda.add(lblResultadosPorPgina);
		
		JLabel label = new JLabel("10");
		label.setBounds(136, 146, 18, 14);
		B�squeda.add(label);
		
		JLabel label_1 = new JLabel("50");
		label_1.setBounds(375, 146, 24, 14);
		B�squeda.add(label_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.inactiveCaption);
		tabbedPane.addTab("B�squeda b�sica", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 389, 141);
		panel_1.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JLabel lblTexto_1 = new JLabel("Texto:");
		lblTexto_1.setBounds(10, 11, 46, 14);
		panel_1.add(lblTexto_1);
		
		JButton button = new JButton("Buscar");
		button.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/search-2-16.png")));
		button.setBounds(294, 188, 105, 23);
		panel_1.add(button);
		
		JButton button_1 = new JButton("Cancelar");
		button_1.setIcon(new ImageIcon(Eclipse.class.getResource("/imagenes/cancel-16.png")));
		button_1.setBounds(179, 188, 105, 23);
		panel_1.add(button_1);
	}
}
