package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		/*
		 * El programa consiste en introducir un n�mero entero, y te dir� si es un n�mero primo o no lo es. Hace la comprobaci�n
		 * 	mediante un bucle dividiendo el numeroLeido desde el 1 hasta el propio numeroLeido, y si el resto es 0, se suma
		 * 	una unidad al contador. Una vez comprobados todos n�meros, si el contador es mayor que 2, te dir� que no es un 
		 * 	n�mero primo, y si no, te confirmar� que s� que es un n�mero primo.
		 */
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
