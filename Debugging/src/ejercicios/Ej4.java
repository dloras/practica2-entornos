package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			if (i != 0) {
				resultadoDivision = numeroLeido / i;
				System.out.println("el resultado de la division es: " + resultadoDivision);
			}
			else {
				System.out.println("el resultado de la divisi�n es: infinito");
			}
		}
		
		
		lector.close();
	}

}
